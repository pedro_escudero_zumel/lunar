require_relative "rover.rb"

require "minitest/autorun"

class TestRover < Minitest::Test
	
	def setup
    	@rover = Rover.new(1, 1, 'N')
	end

	def test_simple_scenario
		@rover.operate('M')
		assert_equal '1 2 N', @rover.position
	end

	def test_left
		@rover.operate('L')
		assert_equal '1 1 W', @rover.position
	end

	def test_right
		@rover.operate('R')
		assert_equal '1 1 E', @rover.position
	end	

	def test_360
		(0..3).each{@rover.operate('R')}
		assert_equal '1 1 N', @rover.position
	end	

	def test_180
		(0..1).each{@rover.operate('R')}
		assert_equal '1 1 S', @rover.position
	end	

end
