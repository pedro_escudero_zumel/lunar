

##Running the code##

From command line (in the folder of the project):

irb run.rb "5 5" "1 2 N" "LMLMLMLMM" "3 3 E" "MMRMMRMRRM"

From irb command line:

load "lunar.rb"

Lunar.new("5 5","1 2 N", "LMLMLMLMM","3 3 E","MMRMMRMRRM")


##Test##

Run test as follow (in the folder of the project):

irb test_rover.rb

irb test_lunar.rb

###Other Posibilities###

The code do not consider the following scenarios:

-Collision between rovers because of bad instructions.

-Fall out of the plateau because of bad instructions.

-Incomplete pair of instructions for a rover (initial position or movements).