class Rover

	ORIENTATIONS = {N:0, E:1, S:2, W:3}
	TURN = {R: '+', L: '-'}
	MOVEMENTS = {N:'+', S:'-', W:'-', E:'+'}

	def initialize (position_x, position_y, oriented)
		@oriented = oriented
		@position_x = position_x.to_i
		@position_y = position_y.to_i 
	end	

	def operate(operation)
		operation == 'M' ?  move : change_orientation(operation)	
	end

	def position
		"#{@position_x} #{@position_y} #{@oriented}"
	end	

	private

	def move
		@oriented == 'N' || @oriented == 'S' ? move_y : move_x
	end	

	def move_x
		@position_x = @position_x.send(MOVEMENTS[@oriented.to_sym], 1)
	end	

	def move_y
		@position_y = @position_y.send(MOVEMENTS[@oriented.to_sym], 1) 
	end	

	def change_orientation(operation)
		@oriented  = take_orientation(operation).map{|num| ORIENTATIONS.key(num) ? ORIENTATIONS.key(num) : orientation_overload(num)}.join.to_s
	end

	def take_orientation(operation)
		[ORIENTATIONS[@oriented.to_sym].send(TURN[operation.to_sym], 1)]
	end	

	def orientation_overload(num)
		num < 0 ? ORIENTATIONS.key(3): ORIENTATIONS.key(0)
	end	

end