require_relative 'rover.rb'

class Lunar

	def initialize(position, *rovers)
		@position_x, @position_y = position.split(" ")
		@rovers = *rovers.each_slice(2)
		@rover_positions = []
		move_rovers if operations_allowed?
	end

	def inspect
		"#{@rover_positions.join(" ")}"
	end	

	private

	def move_rovers
		@rovers.each do |rover|
			position_x, position_y, orientation = rover[0].split(" ")
			current_rover = Rover.new(position_x, position_y, orientation)	
			rover[1].split("").each do |operation|	
				current_rover.operate(operation)
				current_rover.position
			end	
			@rover_positions.push(current_rover.position)
		end
	end	

	def operations_allowed?
		@rovers.map do |rover| 
			rover[1].split("").any?{|m| m.match(/[^MLR]/)}
		end.include?(true) ? false : true
	end	

end